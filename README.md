﻿# FrosTool
Herramienta de monitoreo de variables climaticas para la generación de alertas tempranas para mitigar los efectos de las heladas en los cultivos del departamento de Boyaca- Colombia.

## Árbol de directorios
- BOM: Lista de materiales
- images: Imagenes y capturas de pantalla del proyecto.
- lib: librerias externas usadas en el pŕoyecto.
- PCB: Archivos pdf o gerber del circuito impreso PCB.
- Schematic: Archivos del esquematico, archivos pdf y fritzing.

## LICENCE
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

- @fandres323
- @maurinc2010


