EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "CONECTOR TARJETA SD"
Date "2020-05-17"
Rev "1.0"
Comp "ECOTEC"
Comment1 "Marlon Moreno"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CPU-rescue:Micro_SD_Card_Det_Hirose_DM3AT-Connector J3
U 1 1 5C728306
P 6950 2350
AR Path="/5C728306" Ref="J3"  Part="1" 
AR Path="/5C727FE3/5C728306" Ref="J3"  Part="1" 
F 0 "J3" H 6900 3167 50  0000 C CNN
F 1 "Micro_SD_Card_Det_Hirose_DM3AT" H 6900 3076 50  0000 C CNN
F 2 "Connector_Card:microSD_HC_TFP09-2-12B" H 9000 3050 50  0001 C CNN
F 3 "https://www.hirose.com/product/en/download_file/key_name/DM3/category/Catalog/doc_file_id/49662/?file_category_id=4&item_id=195&is_series=1" H 6950 2450 50  0001 C CNN
	1    6950 2350
	1    0    0    -1  
$EndComp
Text GLabel 3450 2050 0    50   Input ~ 0
SD_CS
Text GLabel 3450 2150 0    50   Input ~ 0
SD_MOSI
Text GLabel 3450 2350 0    50   Input ~ 0
SD_CLK
Text GLabel 3450 2550 0    50   Input ~ 0
SD_MISO
$Comp
L power:GNDD #PWR018
U 1 1 5C728313
P 5950 3350
F 0 "#PWR018" H 5950 3100 50  0001 C CNN
F 1 "GNDD" H 5954 3195 50  0000 C CNN
F 2 "" H 5950 3350 50  0001 C CNN
F 3 "" H 5950 3350 50  0001 C CNN
	1    5950 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2250 5950 2250
Wire Wire Line
	5950 2250 5950 1350
Wire Wire Line
	6050 2450 5950 2450
Wire Wire Line
	6050 2750 5950 2750
Wire Wire Line
	5950 2450 5950 2750
Connection ~ 5950 2750
Wire Wire Line
	5950 2750 5950 2850
$Comp
L power:GNDD #PWR019
U 1 1 5C728383
P 7900 3350
F 0 "#PWR019" H 7900 3100 50  0001 C CNN
F 1 "GNDD" H 7904 3195 50  0000 C CNN
F 2 "" H 7900 3350 50  0001 C CNN
F 3 "" H 7900 3350 50  0001 C CNN
	1    7900 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2850 7750 2850
Wire Wire Line
	7900 2850 7900 3350
Text GLabel 5950 1200 1    50   Input ~ 0
CPU_3.3
Wire Wire Line
	3450 2550 5750 2550
Wire Wire Line
	3450 2350 6050 2350
Wire Wire Line
	3450 2150 4700 2150
Wire Wire Line
	3450 2050 6050 2050
$Comp
L Device:C C3
U 1 1 5EB8F199
P 5050 1650
F 0 "C3" H 5165 1696 50  0000 L CNN
F 1 "100nF" H 5165 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 5088 1500 50  0001 C CNN
F 3 "~" H 5050 1650 50  0001 C CNN
	1    5050 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 1500 5050 1350
Wire Wire Line
	5050 1350 5750 1350
Connection ~ 5950 1350
Wire Wire Line
	5950 1350 5950 1200
Wire Wire Line
	5050 1800 5050 2750
Wire Wire Line
	5050 2750 5950 2750
Wire Wire Line
	6050 2850 5950 2850
Connection ~ 5950 2850
Wire Wire Line
	5950 2850 5950 3350
$Comp
L Device:R R10
U 1 1 5EC232F6
P 5750 1650
F 0 "R10" H 5820 1696 50  0000 L CNN
F 1 "10k" H 5820 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5680 1650 50  0001 C CNN
F 3 "~" H 5750 1650 50  0001 C CNN
	1    5750 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5EC2384C
P 4700 1650
F 0 "R9" H 4770 1696 50  0000 L CNN
F 1 "10k" H 4770 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4630 1650 50  0001 C CNN
F 3 "~" H 4700 1650 50  0001 C CNN
	1    4700 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1500 4700 1350
Wire Wire Line
	4700 1350 5050 1350
Connection ~ 5050 1350
Wire Wire Line
	4700 1800 4700 2150
Connection ~ 4700 2150
Wire Wire Line
	4700 2150 6050 2150
Wire Wire Line
	5750 1800 5750 2550
Connection ~ 5750 2550
Wire Wire Line
	5750 2550 6050 2550
Wire Wire Line
	5750 1500 5750 1350
Connection ~ 5750 1350
Wire Wire Line
	5750 1350 5950 1350
NoConn ~ 6050 1950
NoConn ~ 6050 2650
$EndSCHEMATC
