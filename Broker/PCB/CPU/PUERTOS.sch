EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x06_Female J5
U 1 1 5C7CDB5E
P 3150 1500
F 0 "J5" H 3178 1476 50  0000 L CNN
F 1 "PCIE" H 3178 1385 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x06_P1.00mm_Vertical" H 3150 1500 50  0001 C CNN
F 3 "~" H 3150 1500 50  0001 C CNN
	1    3150 1500
	1    0    0    -1  
$EndComp
Text GLabel 2600 1300 0    50   Input ~ 0
PCIE_TXN0
Text GLabel 2600 1400 0    50   Input ~ 0
PCIE_TXP0
Text GLabel 2600 1500 0    50   Input ~ 0
PCIE_RXP0
Text GLabel 2600 1600 0    50   Input ~ 0
PCIE_RXN0
Text GLabel 2600 1700 0    50   Input ~ 0
PCIE_CKN0
Text GLabel 2600 1800 0    50   Input ~ 0
PCIE_CKP0
Wire Wire Line
	2600 1800 2950 1800
Wire Wire Line
	2950 1700 2600 1700
Wire Wire Line
	2600 1600 2950 1600
Wire Wire Line
	2600 1500 2950 1500
Wire Wire Line
	2950 1400 2600 1400
Wire Wire Line
	2600 1300 2950 1300
$Comp
L Connector:Conn_01x04_Female J6
U 1 1 5C7CE258
P 3150 2200
F 0 "J6" H 3177 2176 50  0000 L CNN
F 1 "ETHERNET" H 3177 2085 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x04_P1.00mm_Vertical" H 3150 2200 50  0001 C CNN
F 3 "~" H 3150 2200 50  0001 C CNN
	1    3150 2200
	1    0    0    -1  
$EndComp
Text GLabel 2550 2100 0    50   Input ~ 0
RSIP0
Text GLabel 2550 2200 0    50   Input ~ 0
RSIN0
Text GLabel 2550 2400 0    50   Input ~ 0
TXON0
Text GLabel 2550 2300 0    50   Input ~ 0
TXOP0
Wire Wire Line
	2950 2400 2550 2400
Wire Wire Line
	2550 2300 2950 2300
Wire Wire Line
	2950 2200 2550 2200
Wire Wire Line
	2550 2100 2950 2100
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5C7D3DF9
P 3150 2750
AR Path="/5C7D3DF9" Ref="J?"  Part="1" 
AR Path="/5C7CD93A/5C7D3DF9" Ref="J7"  Part="1" 
F 0 "J7" H 3177 2776 50  0000 L CNN
F 1 "SERIAL_1" H 3177 2685 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3150 2750 50  0001 C CNN
F 3 "~" H 3150 2750 50  0001 C CNN
	1    3150 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5C7D3E00
P 3150 3200
AR Path="/5C7D3E00" Ref="J?"  Part="1" 
AR Path="/5C7CD93A/5C7D3E00" Ref="J8"  Part="1" 
F 0 "J8" H 3177 3226 50  0000 L CNN
F 1 "SERIAL_2" H 3177 3135 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3150 3200 50  0001 C CNN
F 3 "~" H 3150 3200 50  0001 C CNN
	1    3150 3200
	1    0    0    -1  
$EndComp
Text GLabel 2700 3100 0    50   Input ~ 0
GPIO21
Text GLabel 2700 3200 0    50   Input ~ 0
GPIO20
Wire Wire Line
	2700 3100 2950 3100
Wire Wire Line
	2950 3200 2700 3200
$Comp
L power:GNDD #PWR?
U 1 1 5C7D3E0B
P 2850 3400
AR Path="/5C7D3E0B" Ref="#PWR?"  Part="1" 
AR Path="/5C7CD93A/5C7D3E0B" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 2850 3150 50  0001 C CNN
F 1 "GNDD" H 2854 3245 50  0000 C CNN
F 2 "" H 2850 3400 50  0001 C CNN
F 3 "" H 2850 3400 50  0001 C CNN
	1    2850 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3400 2850 3300
Wire Wire Line
	2850 3300 2950 3300
Text Notes 2850 3100 2    50   ~ 0
RX2\n
Text Notes 2850 3300 2    50   ~ 0
TX2\n
Text GLabel 2700 2650 0    50   Input ~ 0
GPIO45
Text GLabel 2700 2750 0    50   Input ~ 0
GPIO46
Wire Wire Line
	2700 2650 2950 2650
Wire Wire Line
	2700 2750 2950 2750
Wire Wire Line
	2950 2850 2850 2850
Wire Wire Line
	2850 2850 2850 3300
Connection ~ 2850 3300
Text Notes 2850 2650 2    50   ~ 0
TX1
Text Notes 2850 2750 2    50   ~ 0
RX1
$Comp
L Connector:Conn_01x04_Female J17
U 1 1 5C7E3F9B
P 5700 1400
F 0 "J17" H 5728 1376 50  0000 L CNN
F 1 "I2S" H 5728 1285 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x04_P1.00mm_Vertical" H 5700 1400 50  0001 C CNN
F 3 "~" H 5700 1400 50  0001 C CNN
	1    5700 1400
	1    0    0    -1  
$EndComp
Text GLabel 5250 1300 0    50   Input ~ 0
GPIO0
Text GLabel 5250 1400 0    50   Input ~ 0
GPIO1
Text GLabel 5250 1500 0    50   Input ~ 0
GPIO2
Text GLabel 5250 1600 0    50   Input ~ 0
GPIO3
Text GLabel 5250 2150 0    50   Input ~ 0
GPIO4
Text GLabel 5250 2250 0    50   Input ~ 0
GPIO5
Wire Wire Line
	5500 1300 5250 1300
Wire Wire Line
	5250 1400 5500 1400
Wire Wire Line
	5500 1500 5250 1500
Wire Wire Line
	5250 1600 5500 1600
Wire Wire Line
	5500 2150 5250 2150
Wire Wire Line
	5250 2250 5500 2250
$Comp
L Connector:Conn_01x02_Female J18
U 1 1 5C7E534B
P 5700 2150
F 0 "J18" H 5728 2126 50  0000 L CNN
F 1 "I2C" H 5728 2035 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x02_P1.00mm_Vertical" H 5700 2150 50  0001 C CNN
F 3 "~" H 5700 2150 50  0001 C CNN
	1    5700 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J19
U 1 1 5C742643
P 5700 2800
F 0 "J19" H 5727 2776 50  0000 L CNN
F 1 "GPIO" H 5727 2685 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x04_P1.00mm_Vertical" H 5700 2800 50  0001 C CNN
F 3 "~" H 5700 2800 50  0001 C CNN
	1    5700 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J20
U 1 1 5C74279B
P 5700 3400
F 0 "J20" H 5727 3426 50  0000 L CNN
F 1 "PWM" H 5727 3335 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5700 3400 50  0001 C CNN
F 3 "~" H 5700 3400 50  0001 C CNN
	1    5700 3400
	1    0    0    -1  
$EndComp
Text GLabel 5150 2700 0    50   Input ~ 0
GPIO36
Text GLabel 5150 2800 0    50   Input ~ 0
GPIO37
Wire Wire Line
	5150 2800 5500 2800
Wire Wire Line
	5500 2700 5150 2700
Text GLabel 5150 2900 0    50   Input ~ 0
GPIO39
Text GLabel 5150 3000 0    50   Input ~ 0
GPIO40
Wire Wire Line
	5150 3000 5500 3000
Wire Wire Line
	5500 2900 5150 2900
Text GLabel 5150 3400 0    50   Input ~ 0
GPIO19
Wire Wire Line
	5150 3400 5500 3400
$Comp
L Connector:Conn_01x01_Female J13
U 1 1 5C744FB8
P 5650 3750
F 0 "J13" H 5677 3776 50  0000 L CNN
F 1 "GND" H 5677 3685 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5650 3750 50  0001 C CNN
F 3 "~" H 5650 3750 50  0001 C CNN
	1    5650 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J14
U 1 1 5C74509B
P 5650 3900
F 0 "J14" H 5677 3926 50  0000 L CNN
F 1 "GND" H 5677 3835 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5650 3900 50  0001 C CNN
F 3 "~" H 5650 3900 50  0001 C CNN
	1    5650 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J15
U 1 1 5C74510F
P 5650 4050
F 0 "J15" H 5677 4076 50  0000 L CNN
F 1 "GND" H 5677 3985 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5650 4050 50  0001 C CNN
F 3 "~" H 5650 4050 50  0001 C CNN
	1    5650 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J16
U 1 1 5C745182
P 5650 4200
F 0 "J16" H 5677 4226 50  0000 L CNN
F 1 "GND" H 5677 4135 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5650 4200 50  0001 C CNN
F 3 "~" H 5650 4200 50  0001 C CNN
	1    5650 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR033
U 1 1 5C745664
P 5200 4350
F 0 "#PWR033" H 5200 4100 50  0001 C CNN
F 1 "GNDD" H 5204 4195 50  0000 C CNN
F 2 "" H 5200 4350 50  0001 C CNN
F 3 "" H 5200 4350 50  0001 C CNN
	1    5200 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4350 5200 4200
Wire Wire Line
	5200 3750 5450 3750
Wire Wire Line
	5450 3900 5200 3900
Connection ~ 5200 3900
Wire Wire Line
	5200 3900 5200 3750
Wire Wire Line
	5450 4050 5200 4050
Connection ~ 5200 4050
Wire Wire Line
	5200 4050 5200 3900
Wire Wire Line
	5450 4200 5200 4200
Connection ~ 5200 4200
Wire Wire Line
	5200 4200 5200 4050
$Comp
L Connector:Conn_01x01_Female J9
U 1 1 5C7470A2
P 3150 3800
F 0 "J9" H 3178 3826 50  0000 L CNN
F 1 "VCC" H 3178 3735 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 3150 3800 50  0001 C CNN
F 3 "~" H 3150 3800 50  0001 C CNN
	1    3150 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J10
U 1 1 5C7471B1
P 3150 3950
F 0 "J10" H 3178 3976 50  0000 L CNN
F 1 "VCC" H 3178 3885 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 3150 3950 50  0001 C CNN
F 3 "~" H 3150 3950 50  0001 C CNN
	1    3150 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J11
U 1 1 5C747275
P 3150 4100
F 0 "J11" H 3178 4126 50  0000 L CNN
F 1 "VCC" H 3178 4035 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 3150 4100 50  0001 C CNN
F 3 "~" H 3150 4100 50  0001 C CNN
	1    3150 4100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J12
U 1 1 5C7472DA
P 3150 4250
F 0 "J12" H 3178 4276 50  0000 L CNN
F 1 "VCC" H 3178 4185 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 3150 4250 50  0001 C CNN
F 3 "~" H 3150 4250 50  0001 C CNN
	1    3150 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR031
U 1 1 5C747447
P 2600 3700
F 0 "#PWR031" H 2600 3550 50  0001 C CNN
F 1 "+3.3V" H 2615 3873 50  0000 C CNN
F 2 "" H 2600 3700 50  0001 C CNN
F 3 "" H 2600 3700 50  0001 C CNN
	1    2600 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 3700 2600 3800
Wire Wire Line
	2600 4250 2950 4250
Wire Wire Line
	2950 4100 2600 4100
Connection ~ 2600 4100
Wire Wire Line
	2600 4100 2600 4250
Wire Wire Line
	2950 3950 2600 3950
Connection ~ 2600 3950
Wire Wire Line
	2600 3950 2600 4100
Wire Wire Line
	2950 3800 2600 3800
Connection ~ 2600 3800
Wire Wire Line
	2600 3800 2600 3950
Text GLabel 2700 4800 0    50   Input ~ 0
SPI_CS1
Text GLabel 2700 4900 0    50   Input ~ 0
SPI_CLK
Text GLabel 2700 5000 0    50   Input ~ 0
SPI_MISO
Text GLabel 2700 5100 0    50   Input ~ 0
SPI_MOSI
Text GLabel 2700 5200 0    50   Input ~ 0
SPI_CS0
Text GLabel 2700 5300 0    50   Input ~ 0
GPIO11
Wire Wire Line
	2950 4800 2700 4800
Wire Wire Line
	2700 4900 2950 4900
Wire Wire Line
	2950 5000 2700 5000
Wire Wire Line
	2700 5100 2950 5100
Wire Wire Line
	2700 5200 2950 5200
Wire Wire Line
	2950 5300 2700 5300
$Comp
L Connector:Conn_01x06_Female J23
U 1 1 5C89E92B
P 3150 5000
F 0 "J23" H 3177 4976 50  0000 L CNN
F 1 "SPI" H 3177 4885 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x06_P1.00mm_Vertical" H 3150 5000 50  0001 C CNN
F 3 "~" H 3150 5000 50  0001 C CNN
	1    3150 5000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
